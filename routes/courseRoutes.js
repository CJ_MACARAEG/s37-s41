const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/CourseController')
const auth = require('../auth')

// Create single course

router.post('/create', auth.verify, (request, response) => {
	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	CourseController.addCourse(data).then((result) => {
		response.send(result)
	})
})

router.get('/', (request, response) => {
	CourseController.getAllCourses().then((result) => {
		response.send(result)
	})
})

// Get all ACTIVE courses
router.get('/active', (request, response) => {
	CourseController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get single Course
router.get('/:courseId', (request, response) => {
	CourseController.getCourse(request.params.courseId).then((result) => {
		response.send(result)
	})
})

// Update single course
router.patch('/:courseId/update', auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.courseId, request.body).then((result) => {
		response.send(result)
	})
})

// Archieve
router.patch('/:courseId/archieve', auth.verify, (request, response) => {
	CourseController.archieveCourse(request.params.courseId, request.body).then((result) => {
		response.send(result)
	})
})

module.exports = router