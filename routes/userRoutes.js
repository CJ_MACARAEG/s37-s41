const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// Check if email exist
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})

// register
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Get single users details
router.get("/:id/details", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// Enroll a user
router.post('/enroll', auth.verify, (request, response) => {
	let data = {
		userId: request.body.userId,
		courseId: request.body.courseId
	}

	UserController.enroll(data).then((result) => {
		response.send(result)
	}) 
})

module.exports = router